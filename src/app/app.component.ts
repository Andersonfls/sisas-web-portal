import { Component, OnInit } from '@angular/core';
import { ThemeService } from './services/theme.service';
import {Observable} from 'rxjs';
import {Banner, BannerService} from './banner';
import {HttpResponse} from '@angular/common/http';
import {SobreDna} from './sobre-dna/sobre-dna.model';
import {SobreDnaService} from './contactos/sobre-dna.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isThemeDark: Observable<boolean>;
  index = 0;
  speed = 2000;
  banners: Banner[];

  constructor(private themeService: ThemeService,
              private bannerService: BannerService) {}

  ngOnInit() {
    this.isThemeDark = this.themeService.isThemeDark;
    this.loadAllBannerImages();
  }

  loadAllBannerImages() {
    this.bannerService.query().subscribe((res: HttpResponse<Banner[]>) => {
        this.banners = res.body;
      });
  }

}
