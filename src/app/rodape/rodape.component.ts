import { Component, OnInit } from '@angular/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-rodape',
  templateUrl: './rodape.component.html',
  styleUrls: ['./rodape.component.scss']
})
export class RodapeComponent implements OnInit {

  data: string;
  pipe = new DatePipe('en-US'); // Use your own locale
  constructor() { }

  ngOnInit() {
    this.data = this.pipe.transform(Date.now(), 'short');
  }

}
