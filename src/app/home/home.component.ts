import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import {Banner, BannerService} from '../banner';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers:[ NewsService]

})
export class HomeComponent implements OnInit {
  news= {articles:[]};
  index = 0;
  speed = 2000;
  banners: Banner[];

  constructor(private newsService: NewsService,
              private bannerService: BannerService) {}

  ngOnInit() {
    this.newsService.getTopHeadLines()
      .subscribe(
        response => this.news = response.json()
      );
    this.loadAllBannerImages();
  }

  loadAllBannerImages() {
    this.bannerService.query().subscribe((res: HttpResponse<Banner[]>) => {
      this.banners = res.body;
    });
  }
}
