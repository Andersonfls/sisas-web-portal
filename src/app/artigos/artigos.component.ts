import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {SobreDnaService} from '../contactos/sobre-dna.service';
import {ActivatedRoute, Data} from '@angular/router';
import {SobreDna} from '../sobre-dna/sobre-dna.model';
import {NoticiasPortal} from '../posts/noticias-portal.model';
import {NoticiasPortalService} from '../posts/noticias-portal.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-sobre',
  templateUrl: './artigos.component.html',
  styleUrls: ['./artigos.component.scss']
})
export class ArtigosComponent implements OnInit {
  sobre: NoticiasPortal;
  private subscription: Subscription;

  constructor(private service: NoticiasPortalService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe((params) => {
      this.load(params['id']);
    });
  }

  load(id) {
    this.service.find(id).subscribe(
      (res: HttpResponse<SobreDna>) => {
        if (res.body) {
              this.sobre = res.body;
              console.log(this.sobre);
            }
        }
    );
  }

}
