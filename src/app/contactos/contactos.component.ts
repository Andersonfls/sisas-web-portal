import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import {SobreDnaService} from './sobre-dna.service';
import {SobreDna} from './sobre-dna.model';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.scss']
})
export class ContactosComponent implements OnInit {
  contacto: SobreDna;

  constructor(private service: SobreDnaService) {
    // this.contacto = new SobreDna();
    // this.contacto.telemovel = '+ 353 987 6545';
    // this.contacto.endereco = 'Angola, test';
    // this.contacto.descricao = 'Texto sobre contactos!';
    // this.contacto.email = 'email@sisas.com';
  }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    this.service.find(2).subscribe(
      (res: HttpResponse<SobreDna>) => {
        this.contacto = res.body;
        console.log(this.contacto);
      }
    );
  }

}
