import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import {NoticiasPortalService} from './noticias-portal.service';
import {NoticiasPortal} from './noticias-portal.model';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
  providers: [NewsService]
})
export class PostsComponent implements OnInit {
  news = {articles: []};
  newsSources = {sources: []};
  noticiasPortals: NoticiasPortal[];


  constructor(private newsService: NewsService,
              private noticiasPortalService: NoticiasPortalService) {}

  ngOnInit() {
    this.newsService.getTopHeadLines()
      .subscribe(
        response => this.news = response.json()
      );
    this.getnewsSources();
    this.loadAllNoticias();
  }


  loadAllNoticias() {
    this.noticiasPortalService.query().subscribe(
      (res: HttpResponse<NoticiasPortal[]>) => {
        this.noticiasPortals = res.body;
        this.noticiasPortals.forEach(i =>{
          let txt: string = i.texto;
          txt = txt.replace('<p>', '');
          txt = txt.replace('</p>', '');
          i.texto = txt;
        });
        console.log('NOTICIAS ');
        console.log(this.noticiasPortals);
      } );
  }

  filterNews(source) {
    this.news = {articles: [] };
    this.newsService.getNewBySource(source)
      .subscribe(
        response => this.news = response.json()
      );
  }

  getnewsSources() {
    this.newsService.getSources()
      .subscribe(
        response => this.newsSources = response.json()
      );
  }
}
