import { BaseEntity } from '../shared/index';

export class NoticiasPortal implements BaseEntity {
    constructor(
        public id?: number,
        public titulo?: string,
        public texto?: any,
        public imagemCapaContentType?: string,
        public imagemCapa?: any,
        public dataCriacao?: any,
        public dataAlteracao?: any,
        public status?: boolean,
        public url?: string,
    ) {
        this.status = false;
    }
}
