
export class FileResponseModel {
    constructor(
        public id?: number,
        public nomeArquivo?: string,
        public uri?: string,
        public fileType?: string,
        public size?: number,
        public descricao?: string,
        public dataInclusao?: Date,
        public dataAlteracao?: Date,
        public status?: boolean,
        public tipoArquivo?: number
    ) {
    }
}
