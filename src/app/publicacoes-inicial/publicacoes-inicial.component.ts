import { Component, OnInit } from '@angular/core';
import {FileResponseModel} from './uploadFileResponse.model';
import {UploadFileService} from './upload-file.service';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-publicacoes',
  templateUrl: './publicacoes-inicial.component.html',
  styleUrls: ['./publicacoes-inicial.component.scss']
})
export class PublicacoesInicialComponent implements OnInit {
  arquivos: FileResponseModel[];

  constructor(private uploadService: UploadFileService) { }

  ngOnInit() {
    this.uploadService.allFilesPublicaoInicial().subscribe((event) => {
      if (event instanceof HttpResponse) {
        this.arquivos = event.body;
      }
    });
  }

}
