import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {FileResponseModel} from './uploadFileResponse.model';
import {SERVER_API_URL} from '../app.constants';
import {createRequestOption} from '../shared';

@Injectable()
export class UploadFileService {
     private resourceUrl =  SERVER_API_URL + 'api/arquivos-portals';
     private resourceUrl2 =  SERVER_API_URL + 'api/arquivos-portals/publicacoes';
     private resourceUrl3 =  SERVER_API_URL + 'api/arquivos-portals/publicacoes-inicial';
     private resourceUrl4 =  SERVER_API_URL + 'api/arquivos-portals/projectos';

    constructor(private http: HttpClient) {}

    getFiles(): Observable<any> {
        return this.http.get(this.resourceUrl + '/getallfiles');
    }

    allFilesPublicao(req?: any): Observable<HttpResponse<FileResponseModel[]>> {
      const options = createRequestOption(req);
      return this.http.get<FileResponseModel[]>(this.resourceUrl2, { params: options, observe: 'response' })
        .map((res: HttpResponse<FileResponseModel[]>) => this.convertArrayResponse(res));
    }

    allFilesPublicaoInicial(req?: any): Observable<HttpResponse<FileResponseModel[]>> {
      const options = createRequestOption(req);
      return this.http.get<FileResponseModel[]>(this.resourceUrl3, { params: options, observe: 'response' })
        .map((res: HttpResponse<FileResponseModel[]>) => this.convertArrayResponse(res));
    }

    allFilesProjectos(req?: any): Observable<HttpResponse<FileResponseModel[]>> {
      const options = createRequestOption(req);
      return this.http.get<FileResponseModel[]>(this.resourceUrl4, { params: options, observe: 'response' })
        .map((res: HttpResponse<FileResponseModel[]>) => this.convertArrayResponse(res));
    }

    query(req?: any): Observable<HttpResponse<FileResponseModel[]>> {
        const options = createRequestOption(req);
        return this.http.get<FileResponseModel[]>(this.resourceUrl + '/getallfiles', { params: options, observe: 'response' })
            .map((res: HttpResponse<FileResponseModel[]>) => this.convertArrayResponse(res));
    }

    private convertArrayResponse(res: HttpResponse<FileResponseModel[]>): HttpResponse<FileResponseModel[]> {
        const jsonResponse: FileResponseModel[] = res.body;
        const body: FileResponseModel[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to FileResponseModel.
     */
    private convertItemFromServer(file: FileResponseModel): FileResponseModel {
        const copy: FileResponseModel = Object.assign({}, file);
        return copy;
    }
}
