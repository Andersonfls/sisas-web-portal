import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { PostsComponent } from './posts/posts.component';
import { HomeComponent } from './home/home.component';
import {ContactosComponent} from './contactos/contactos.component';
import {SobreComponent} from './sobre-dna/sobre.component';
import {ProjectosComponent} from './projectos/projectos.component';
import {ArtigosComponent} from './artigos/artigos.component';
import {PublicacoesComponent} from './publicacoes/publicacoes.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'posts',
    component: PostsComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'contactos',
    component: ContactosComponent
  },
  {
    path: 'sobre',
    component: SobreComponent,
  },
  {
    path: 'projectos',
    component: ProjectosComponent,
  },
  {
    path: 'artigos',
    component: ArtigosComponent,
  },
  {
    path: 'artigos/:id',
    component: ArtigosComponent,
  },
  {
    path: 'publicacoes',
    component: PublicacoesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
