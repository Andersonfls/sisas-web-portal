//
// import { Injectable } from '@angular/core';
// import { Resolve, ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
// import { Observable } from 'rxjs/Rx';
// import {SobreDnaService} from '../contactos/sobre-dna.service';
// import {HttpResponse} from '@angular/common/http';
// import {SobreDna} from './sobre-dna.model';
//
// @Injectable()
// export class SobreResolve implements Resolve<any> {
//   constructor(private service: SobreDnaService){}
//   resolve(route:ActivatedRouteSnapshot,
//           state:RouterStateSnapshot,
//   ): Observable<any> {
//     return this.service.query().subscribe(
//       (res: HttpResponse<SobreDna[]>) => {
//         if (res.body) {
//           res.body.forEach((i) => {
//             if (i.tipoPagina === 1) { // Sobre
//              return  i;
//             }
//           });
//         }
//       }
//     );
//   }
// }
