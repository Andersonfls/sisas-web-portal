import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SobreDna} from './sobre-dna.model';
import {HttpResponse} from '@angular/common/http';
import {SobreDnaService} from '../contactos/sobre-dna.service';
import {ActivatedRoute, Data} from '@angular/router';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.component.html',
  styleUrls: ['./sobre.component.scss']
})
export class SobreComponent implements OnInit {
  sobre: SobreDna;

  constructor(private service: SobreDnaService,  protected activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    this.service.find(1).subscribe(
      (res: HttpResponse<SobreDna>) => {
        if (res.body) {
              this.sobre = res.body;
              console.log(this.sobre);
            }
        }
    );
  }

}
