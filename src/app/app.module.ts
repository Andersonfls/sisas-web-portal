import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { PostsComponent } from './posts/posts.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ThemeService } from './services/theme.service';
import { NewsService } from './services/news.service';
import {ServiceWorkerModule} from '@angular/service-worker';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import 'hammerjs';
import { PublicacoesInicialComponent } from './publicacoes-inicial/publicacoes-inicial.component';
import { RodapeComponent } from './rodape/rodape.component';
import {BannerService} from './banner';
import {HttpClientModule} from '@angular/common/http';
import {NoticiasPortalService} from './posts/noticias-portal.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ContactosComponent} from './contactos/contactos.component';
import {SobreDnaService} from './contactos/sobre-dna.service';
import {SobreComponent} from './sobre-dna/sobre.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ProjectosComponent} from './projectos/projectos.component';
import {UploadFileService} from './publicacoes-inicial/upload-file.service';
import {ArtigosComponent} from './artigos/artigos.component';
import {PublicacoesComponent} from './publicacoes/publicacoes.component';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    HomeComponent,
    NavbarComponent,
    PublicacoesInicialComponent,
    RodapeComponent,
    ContactosComponent,
    SobreComponent,
    ProjectosComponent,
    ArtigosComponent,
    PublicacoesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgxHmCarouselModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    NgbModule,
    FlexLayoutModule,
  ],
  providers: [ThemeService, NewsService, BannerService, NoticiasPortalService, SobreDnaService, UploadFileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
