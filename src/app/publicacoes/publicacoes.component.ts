import {Component, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {FileResponseModel} from '../publicacoes-inicial/uploadFileResponse.model';
import {UploadFileService} from '../publicacoes-inicial/upload-file.service';

@Component({
  selector: 'app-projectos',
  templateUrl: './publicacoes.component.html',
  styleUrls: ['./publicacoes.component.scss']
})
export class PublicacoesComponent implements OnInit {
  arquivos: FileResponseModel[];
  data: string;
  pipe = new DatePipe('en-US'); // Use your own locale

  constructor(private uploadService: UploadFileService) {
  }

  ngOnInit() {
    this.uploadService.allFilesPublicao().subscribe((event) => {
      if (event instanceof HttpResponse) {
        this.arquivos = event.body;
      }
    });
    this.data = this.pipe.transform(Date.now(), 'short');
    console.log(this.data);
  }

  // loadAll() {
  //   this.service.query().subscribe(
  //     (res: HttpResponse<SobreDna[]>) => {
  //       if (res.body) {
  //             this.sobre = res.body;
  //           }
  //       }
  //   );
  // }

}
