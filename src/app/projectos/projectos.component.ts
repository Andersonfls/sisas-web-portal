import {Component, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {FileResponseModel} from '../publicacoes-inicial/uploadFileResponse.model';
import {UploadFileService} from '../publicacoes-inicial/upload-file.service';

@Component({
  selector: 'app-projectos',
  templateUrl: './projectos.component.html',
  styleUrls: ['./projectos.component.scss']
})
export class ProjectosComponent implements OnInit {
  arquivos: FileResponseModel[];
  data: string;
  pipe = new DatePipe('en-US'); // Use your own locale

  constructor(private uploadService: UploadFileService) {
  }

  ngOnInit() {
    this.uploadService.allFilesProjectos().subscribe((event) => {
      if (event instanceof HttpResponse) {
        this.arquivos = event.body;
      }
    });
    this.data = this.pipe.transform(Date.now(), 'short');
  }

}
